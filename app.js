const program = require('commander');
const fs = require('fs');
const http = require('http');
const Entities = require('html-entities').AllHtmlEntities;
const shell = require('shelljs');

program
  .version('1.0.0')
  .option('-i, --input [type]', 'Input Folder')
  .option('-o, --output [type]', 'Output Folder')
  .parse(process.argv);

const outputFolderName = 'output';
const inputFolderName = 'input';
const friendUrl = 'http://smarthome30.ru/tts-test.php?&q=';
const countWords = 99;
const indexFileName = 'index.txt';
const entities = new Entities();

let inputFolder = (program.input) ? program.input : inputFolderName;
let outputFolder = (program.output) ? program.output : outputFolderName;

// Дописать проверку папок. Что бы они не были пусты. Должен быть один
// txt файл и более одной картинки. Количество можно задать const
// неплохобы при инициализации и отдавать json с txt файлом и картинками

function checkInputFolder(inputFolder) {
  const folders = fs.readdirSync(inputFolder);
  if (folders.length) {
    const inputFolderList = folders.filter((folder) => {
      if (fs.existsSync(`${inputFolder}/${folder}/${indexFileName}`)) {
        return true;
      }
      console.log(`${inputFolder}/${folder}/${indexFileName} - not found!`);
      return false;
    });
    if (inputFolderList.length) {
      console.log(`Input folder list - ${inputFolderList.length}`);
      return inputFolderList;
    } throw new Error('= Input folder is empty =');
  } else throw new Error('= Input folder is empty =');
}

function init() {
  let inputFolderList = [];
  if (fs.existsSync(inputFolder)) {
    console.log('Input folder - ok');
    inputFolderList = checkInputFolder(inputFolder);
  } else {
    console.log('Iutput folder not found!');
    return;
  }

  if (fs.existsSync(outputFolder)) {
    console.log('Output folder - ok');
  } else {
    console.log('Output folder not found!');
    if (fs.mkdirSync(outputFolder)) {
      console.log('Output folder created - ok');
    } else { return; }
  }
  return inputFolderList;
}
// Нужно сделать проверку если предложение больше 99 слов так как разбивка идет по предложениям
function getContent(folder) {
  let resultContent = [''];
  const fileContent = fs.readFileSync(`${inputFolder}/${folder}/${indexFileName}`, 'utf8');
  const arrContent = fileContent.match(/[^\.!\?]+[\.!\?]+["']?|\s*$/g);
  for (let i = 0; i < arrContent.length - 1; i++) {
    if ((resultContent[resultContent.length - 1] + arrContent[i]).split(' ').length < countWords) {
      resultContent[resultContent.length - 1] = resultContent[resultContent.length - 1] + arrContent[i];
    } else {
      resultContent.push(arrContent[i]);
    }
  }
  console.log(`${inputFolder}/${folder}/${indexFileName} get content - ok!`);
  return resultContent;
};

function getVoice(arrContent, folder) {
  Promise.all(
    arrContent.map((content, i) => {
      const fileVoice = fs.createWriteStream(`${inputFolder}/${folder}/${i}.mp3`);
      http.get(`${friendUrl}${encodeURI(content.replace(/\s+/g,' ').trim())}`, (response) => {
        response.pipe(fileVoice);
      });
      return new Promise((resolve, reject) => {
        fileVoice.on('finish', () => {
          fileVoice.close(); // close() is async, call cb after close completes.
          console.log(`${inputFolder}/${folder}/${i}.mp3`);
          resolve(`${inputFolder}/${folder}/${i}.mp3`);
        }).on('error', (err) => { // Handle errors
          fs.unlink(`${inputFolder}/${folder}/${i}.mp3`);
          reject(err);
          console.log(`Error ${folder}/${i}.mp3 - not found!`);
        });
      });
    }),
  ).then(() => { console.log(`${folder} - ok`); if (catVoiceFiles(folder)) { return true; }; return false; });
}
function ffmpegVideo(folder) {
  //folder =  folder.replace(/[\\$'" ]/g, "\\$&");
  //ffmpeg -r:v 1/5 -i "image_%04d.jpg" -i voice.mp3 -r:v 30 -codec:v libx264  -preset veryslow -pix_fmt yuv420p -crf 28 -an "out.mp4"

//ffmpeg -i "image_%04d.jpg" -i voice.mp3 -b 20M -s 1920x1080 -shortest  -c:v libx264 -y -sn
//let ffmpegCom =  `ffmpeg -y -r:v 1/5 -i ${inputFolder}/${folder}/image_%04d.jpg -i ${inputFolder}/${folder}/voice.mp3  -filter_complex  "zoompan=z='min(max(zoom,pzoom)+0.0015,1.5)':d=1:x='iw/2-(iw/zoom/2)':y='ih/2-(ih/zoom/2)'"  -r:v 30 -codec:v mpeg4 -preset veryslow -pix_fmt yuv420p -crf 28  ${outputFolder}/${folder}.avi`;
// -filter_complex "scale=8000x4000,zoompan=z='min(zoom+0.002,1.5)':x='iw/2-(iw/zoom/2)':y='ih/2-(ih/zoom/2)'"   -c:v libx264  -preset veryslow -pix_fmt yuv420p -b 20M -y
let ffmpegCom = `ffmpeg  -loop 1 -r:v 1/4 -pattern_type glob -i "${inputFolder}/${folder}/*.jpg"   -i ${inputFolder}/${folder}/voice.mp3 -shortest -start_number 0 -codec:v mpeg4  -pix_fmt yuv420p -s 4096x1716 -b 20M -y ${outputFolder}/${folder}.avi`;
console.log(ffmpegCom);
  if (shell.exec(ffmpegCom).code !== 0) {
      shell.echo('Error: ffmpeg');
      shell.exit(1);
    }
    return;
  console.log(ffmpegCom);
}

function catVoiceFiles(folder) {
  if (!folder) { return false; }

  folder = folder.replace(/[\\$'" ]/g, "\\$&");
  const catCommand = `cat ${inputFolder}/${folder}/*.mp3 > ${inputFolder}/${folder}/voice.mp3`;
  fs.watchFile(`${inputFolder}/${folder}/voice.mp3`, (event, filename) => {
    console.log(`${inputFolder}/${folder}/voice.mp3 - watch`);
    if (filename) {
      ffmpegVideo(folder);
      fs.unwatchFile(`${inputFolder}/${folder}/voice.mp3`);
    }
  });
  if (shell.exec(catCommand).code !== 0) {
    shell.echo('Error: cat');
  //  shell.exit(1);
  //  return false;
  }
}

try {
  const inputFolderList = init();
  if (inputFolderList && inputFolderList.length) {
    inputFolderList.forEach((folder) => {
      const arrContent = getContent(folder);
     getVoice(arrContent, folder);
    // catVoiceFiles(folder)
    });
  }

} catch (e) {
  console.log(e);
}
